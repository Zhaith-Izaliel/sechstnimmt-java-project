# Author
RIBEYRE Virgil

# Information
It's the repository of the Sechst Nimmt! java project in L2 MIAGE first semester.

# Rules of the game
You can find the rules (in french) within every PDF files in the "Rules-Guidelines" folder at the root of the repository.

# Project Structure
You can find the source files in `./code/src`, the executables files in `./code/bin`, the used API in `./code/lib`, the Junit tests in `./code/test`, the executable jar file in `./jar`, the javadoc in `./javadoc` and the report in `./report` ! :)
