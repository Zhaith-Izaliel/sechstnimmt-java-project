import org.junit.Test;
import static org.junit.Assert.assertEquals;
import ribeyre.sechstnimmt_meta.*;
import ribeyre.sechstnimmt.*;
import ribeyre.utils.*;

public class TestJunit {

   @Test
   public void testCreateDeck() {
      Game.createDeckForTesting();
      Card[] gettedDeck = Game.getDeck();
      for(int i=0; i<104;i++) {
         assertEquals(gettedDeck[i].getPoints(),i+1);
         if (i+1 == 55) {
           assertEquals(gettedDeck[i].getJam(),6);
         } else if((i+1)%10 == 0) {
           assertEquals(gettedDeck[i].getJam(),3);
         } else if((i+1)%5 == 0) {
           assertEquals(gettedDeck[i].getJam(),2);
         } else if((i+1)%11 == 0) {
           assertEquals(gettedDeck[i].getJam(),5);
         } else {
           assertEquals(gettedDeck[i].getJam(),1);
         }
      }
   }

   @Test
   public void testCreateDeckFirstVariation() {
      for(int i=2; i<11; i++) {
         Game.setPlayerNb(i);
         Game.createDeckForTestingFirstVariation();
         Card[] gettedDeck = Game.getDeck();
         for(int j=0; j<gettedDeck.length;j++) {
            assertEquals(gettedDeck[i].getPoints(),j+1);
         }
      }
   }
}
