package ribeyre.sechstnimmt;

/** Class which define a card.
 * @author RIBEYRE Virgil
 */
public class Card {
  private int points; //Card's value.
  private int jam; //Number of jam points on the card.

  /**
   * Default empty Card's constructor.
   */
  public Card() {

  }
  /**
  * Default Card's constructor.
  * @param points int which is the value of the card.
  * @param jam    int which is the jam points of the card.
  */
	public Card(int points, int jam) {
		this.points = points;
		this.jam = jam;
	}

	/**
	* Returns value of points
	* @return an int.
	*/
	public int getPoints() {
		return this.points;
	}

	/**
	* Returns value of jam
	* @return an int.
	*/
	public int getJam() {
		return this.jam;
	}

	/**
	* Sets new value of points
	* @param points which is an int.
	*/
	public void setPoints(int points) {
		this.points = points;
	}

	/**
	* Sets new value of jam
	* @param jam which is an int.
	*/
	public void setJam(int jam) {
		this.jam = jam;
	}

	/**
	* Create string representation of Card for printing.
	* @return a String
	*/
	@Override
	public String toString() {
		return "[pts=" + this.getPoints() + ", jam=" + this.getJam() + "]";
	}
}
