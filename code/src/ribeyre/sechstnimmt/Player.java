package ribeyre.sechstnimmt;
import java.util.Scanner;
import org.apache.commons.lang3.ArrayUtils;
import ribeyre.utils.*;

/**
 * Class which define a Player.
 * @author RIBEYRE Virgil
 */

public class Player {
  private String name;
  private Card hand[];
  private int jamPoints;
	private Card choosedCard;

	/**
	 * Initialising constructor of Player.
	 * @param name      String which defines the name of the player.
	 */
	public Player(String name) {
		this.name = name;
	}

	/**
	 * Display hand, jams and possible action to the player and ask him which one would he wants to do.
	 * @param scan A Scanner to prevent memory leak and InputStream to close unexpectedly.
	 */
	public void doAnAction(Scanner scan) {
		//Print the menu.
		System.out.println("This is your turn, " + this.name + ".");

    Card choosedCard = new Card();
		boolean rightCard = false;
		while(!rightCard) {
			System.out.println("Which card do you want to choose ?");
			int choice = ScannerHandlerException.scanInt(scan);
			if(choice > this.getHand().length) {
				ConsoleUtils.errorMessageInput(scan);
				this.displayHand();
			} else {
				this.playACard(choice);
				rightCard = true;
			}
		}
		ConsoleUtils.clear();
	}

	/**
	 * Display the hand of the player following this patern: (by ascending order)
	 * Card 1: [pts=*, jam=*] | Card 2: [pts=*, jam=*] ...
	 */
	public void displayHand() {
		int cardOrdinal;

		//Sorting the hand:
		this.setHand(ArrayTools.sortCardArray(this.getHand()));

		//Displaying the hand.
		System.out.println("Here is your hand:");
		for(int i=0;i<this.getHand().length;i++) {
			cardOrdinal = i+1;
			if(i == (this.getHand().length)/2) {
				System.out.println("");
			}
			if(i == this.getHand().length-1) {
				System.out.print("Card " + cardOrdinal +" "+ this.getHand()[i].toString());
			} else {
				System.out.print("Card " + cardOrdinal +" "+ this.getHand()[i].toString()+ " | ");
			}
		}
		System.out.println("");
	}

	/**
	 * Method to choose and play a card in the hand of the player.
	 * @param indexChoosed the index of the card in the hand.
	 */
  public void playACard(int indexChoosed) {
      this.choosedCard = this.getHand()[indexChoosed-1];
  		this.setHand(ArrayUtils.remove(this.getHand(),indexChoosed-1));
	}

	/**
	 * Method to check if the hand is empty.
	 * @return true if the hand is empty, false otherwise.
	 */
	public boolean isHandEmpty() {
		boolean returnedBool=false;
		if(this.hand.length == 0 ){
			returnedBool = true;
		}
		return returnedBool;
	}

	/**
	 * Method to show to the player his jams.
	 */
	public void showJamPoints() {
		System.out.println("You have " + this.getJamPoints() + " Jams.");
	}

	/**
	 * Method to add a Card in the player's hand.
	 * @param aCard a Card to had in the player's hand.
	 */
  public void addCardToHand(Card aCard) {
    this.hand = ArrayUtils.add(this.hand, aCard);
  }
	/**
	* Returns value of name
	* @return a String.
	*/
	public String getName() {
		return this.name;
	}

	/**
	* Returns value of hand
	* @return a table of Card.
	*/
	public Card[] getHand() {
		return this.hand;
	}

	/**
	* Returns value of jamPoints
	* @return an int.
	*/
	public int getJamPoints() {
		return this.jamPoints;
	}

	/**
	 * Get the value of choosedCardForTurnPlace.
	 * @return a Card.
	 */
	public Card getChoosedCard() {
		return this.choosedCard;
	}

	/**
	* Sets new value of name
	* @param name which is a String.
	*/
	public void setName(String name) {
		this.name = name;
	}

	/**
	* Sets new value of hand
	* @param hand which is a table of Card object.
	*/
	public void setHand(Card[] hand) {
		this.hand = hand;
	}

	/**
	* Sets new value of jamPoints.
	*/
	public void setJamPoints(int sumJams) {
		this.jamPoints = sumJams;
	}

	/**
	 * Sets new value of choosedCardForTurnPlace
	 * @param aCard which is a card.
	 */
	public void setChoosedCard(Card aCard) {
		this.choosedCard = aCard;
	}
}
