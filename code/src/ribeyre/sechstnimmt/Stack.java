package ribeyre.sechstnimmt;
import org.apache.commons.lang3.ArrayUtils;
import ribeyre.utils.*;

/**
 * Class which define a Stack.
 * @author RIBEYRE Virgil
 */
public class Stack {
	private final int max=5; //Max number of Cards.
  protected Card[] table;

	/**
	 * Constructor to create a Stack with a Card passed as parameter.
	 * @param aCard a Card to initialize the stack.
	 */
	public Stack(Card aCard) {
		this.table = ArrayUtils.add(this.table, aCard);
	}

	/**
	 * Methods which add a Card at the end of the Stack only if the value of the passed card is greater than the value of the last card of the stack.
	 * @param aCard Card object.
	 */
	public void addCardToStack(Card aCard) {
			this.table = ArrayUtils.add(table, aCard);
	}

	/**
	 * Method to sort the stack using sortCardArray from ArrayTools.
	 */
	public void sortStack() {
		this.table = ArrayTools.sortCardArray(table);
	}

	/**
	 * Return the difference between the value of the passed Card and the last card added to the stack.
	 * @param  aCard which is a non-empty Card.
	 * @return       an int which is the difference between the value of the passed Card and the value of the last Card of the Stack.
	 */
	public int differencePoints(Card aCard) {
		int lastIndex = this.table.length - 1;
		return aCard.getPoints() - this.table[lastIndex].getPoints();
	}

	/**
	 * Method which empty the stack and return the sum of jams on removed Cards.
	 * @return an int which is the sum of jams.
	 */
	public int emptyStack() {
		int jamSum = 0;
		for(int i=0; i<this.table.length; i++) {
			jamSum += this.table[i].getJam();

		}
		this.table = new Card[0];
		return jamSum;
	}

	/**
	 * Method which check if the Stack is full.
	 * @return a boolean. True if the Stack is full, false otherwise.
	 */
	public boolean isStackFull() {
		return this.table.length == this.getMax();
	}

	/**
	 * Method to define if the card passed as parameter is Greater than the last card of the stack.
	 * @param  aCard a Card.
	 * @return       true if it's the case, false otherwise.
	 */
	public boolean isGreaterThanLastCard(Card aCard) {
		return this.table[this.table.length-1].getPoints() < aCard.getPoints();
	}

	/**
	 * Display the content of the Stack.
	 */
	public void displayTable() {
		for(int i=0; i<this.table.length; i++) {
			if(i+1 == this.table.length) {
				System.out.print("Card " + (i+1) + " " + table[i].toString());
			} else {
				System.out.print("Card " + (i+1) + " " + table[i].toString() + " || ");
			}
		}
		System.out.println("");
	}

	/**
	* Returns value of max
	* @return int which is the size of table.
	*/
	public int getMax() {
		return this.max;
	}

	/**
	* Returns value of table
	* @return a Card array.
	*/
	public Card[] getTable() {
		return this.table;
	}

	/**
	* Sets new value of table
	* @param table a Card array.
	*/
	public void setTable(Card[] table) {
		this.table = table;
	}
}
