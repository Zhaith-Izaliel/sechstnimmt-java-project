package ribeyre.utils;
import java.io.IOException;
import java.util.Scanner;

public class ConsoleUtils {
  /**
   * Method to clear the console.
   */
  public static void clear() {
    try {
        System.out.print("\033[H\033[2J"); //clear for Unix users.
    } catch (final Exception e) {
        System.out.println(e);
    }
  }

  /**
   * Method to print an error Message and clear the console.
   * @param scan A Scanner to prevent memory leak and InputStream to close unexpectedly.
   */
  public static void errorMessageInput(Scanner scan) {
    ConsoleUtils.clear();
    System.out.println("---ERROR---");
    System.out.println("This input is not a valid answer.");
    System.out.println("Press enter to continue.");
    scan.nextLine();
    scan.nextLine();
    ConsoleUtils.clear();
  }
}
