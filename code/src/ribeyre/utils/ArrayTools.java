package ribeyre.utils;
import ribeyre.sechstnimmt.*;

/**
 * Utility class to sort arrays of Players or Cards.
 * @author RIBEYRE Virgil
 */

public class ArrayTools {
  /**
   * Method to sort an array of card by value (ascending order).
   * @param  passedArray an array of Cards not already sorted.
   * @return             an array of Cards sorted.
   */
  public static Card[] sortCardArray(Card[] passedArray) {
    Card temp;
    int jMin;
    for(int i=0; i<passedArray.length-1; i++) {
      jMin = i; //assume the min is the first element.
      for(int j=i+1; j<passedArray.length; j++) {
        if(passedArray[j].getPoints() < passedArray[jMin].getPoints()) {
          jMin = j;
        }
      }

      if(jMin != i) {
        temp = passedArray[i];
        passedArray[i] = passedArray[jMin];
        passedArray[jMin] = temp;
      }
    }
    return passedArray;
  }

  /**
   * Method to sort an array of players by choosedCardForTurnPlace points (ascending order).
   * @param  passedArray an array of Players not already sorted.
   * @return             an array of Players sorted.
   */
  public static Player[] defineTurnPlace(Player[] passedArray) {
    Player temp;
    int jMin;
    for(int i=0; i<passedArray.length-1; i++) {
      jMin = i; //assume the min is the first element.
      for(int j=i+1; j<passedArray.length; j++) {
        if(passedArray[j].getChoosedCard().getPoints() < passedArray[jMin].getChoosedCard().getPoints()) {
          jMin = j;
        }
      }

      //Swap until the ith element is at the right place.
      if(jMin != i) {
        temp = passedArray[i];
        passedArray[i] = passedArray[jMin];
        passedArray[jMin] = temp;
      }
    }
    return passedArray;
  }

  /**
   * Function to return the minimal of an Array.
   * @param  passedArray Array of Player
   * @return             an int as the minilmum.
   */
  public static int minimalInArray(int[] passedArray) {
    int minimal = passedArray[0];
    for(int i=1;i<passedArray.length;i++) {
      if( minimal > passedArray[i]) {
        minimal = passedArray[i];
      }
    }
    return minimal;
  }

  /**
   * Define the scoring.
   * @param  passedArray A Players array.
   * @return             Return a sort player Array by jam points.
   */
  public static Player[] defineScoring(Player[] passedArray) {
    Player temp;
    int jMin;
    for(int i=0; i<passedArray.length-1; i++) {
      jMin = i; //assume the min is the first element.
      for(int j=i+1; j<passedArray.length; j++) {
        if(passedArray[j].getJamPoints() < passedArray[jMin].getJamPoints()) {
          jMin = j;
        }
      }

      //Swap until the ith element is at the right place.
      if(jMin != i) {
        temp = passedArray[i];
        passedArray[i] = passedArray[jMin];
        passedArray[jMin] = temp;
      }
    }
    return passedArray;
  }
}
