package ribeyre.utils;
import java.io.IOException;
import java.util.Scanner;
import ribeyre.utils.*;

public class ScannerHandlerException {

  /**
   * Method to use a scan.nextInt() which can handle bad entries.
   * @param scan A Scanner to prevent memory leak and InputStream to close unexpectedly.
   * @return     the choosed int.
   */
  public static int scanInt(Scanner scan) {
    boolean validChoice=false;
    int choice=0;
    while(!validChoice) {
      try {
        choice = scan.nextInt();
        validChoice = true;
      } catch(Exception e) {
        validChoice = false;
        ConsoleUtils.errorMessageInput(scan);
      }
    }
    return choice;
  }

  /**
   * Method to use a scan.nextLine() which can handle bad entries.
   * @param scan A Scanner to prevent memory leak and InputStream to close unexpectedly.
   * @return     a String.
   */
  public static String scanString(Scanner scan) {
    boolean validChoice=false;
    String choice="";
    while(!validChoice) {
      try {
        scan.nextLine();
        choice = scan.next();
        validChoice = true;
      } catch(Exception e) {
        validChoice = false;
        ConsoleUtils.errorMessageInput(scan);
      }
    }
    return choice;
  }
}
