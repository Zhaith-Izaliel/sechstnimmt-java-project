package ribeyre.sechstnimmt_meta;
import java.util.Scanner;
import org.apache.commons.lang3.ArrayUtils;
import ribeyre.sechstnimmt.*;
import ribeyre.utils.*;

/**
 * Meta-class which govern the game.
 * @author RIBEYRE Virgil
 */

/* TODO Extension of the game and variation of it.
 *
 */
public class Game {
  private static final int DECKSIZE = 104; //size of the deck. Always constant.
  private static Card[] deck;
  private static final int HANDSIZE = 10; //size of the hand, always constant.
  private static final int ALLSTACKSSIZE = 4; //number of stacks, always constant.
  private static int playerNb;
  private static Player[] allPlayers;
  private static Stack[] allStacks;

  /**
   * Method to run the game !
   */
  public static void letsPlay() {
    //Creating only one scanner for every functions to prevent InputStream to close permanently and memory leaks.
    Scanner scan = new Scanner(System.in);

    //Creating players.
    Game.chooseNumberOfPlayers(scan);
    Game.createPlayers(scan);
    int[] game = Game.displayMenu(scan);

    //choosing the game mode with the returned value of displayMenu.
    if(game[0] == 1) //Regular Game.
    {
        //---BEGINING OF THE GAME---
        boolean isGameEnded = false;
        int roundNumber = 1;
        while(!isGameEnded) {
          if(game[1] == 1) {
            //creating deck.
            Game.createDeck();
            //creating players' hand.
            for(int i=0; i<Game.allPlayers.length; i++) {
              Game.allPlayers[i].setHand(Game.createHand());
            }
          } else if(game[1] == 2) {
            //creating deck for first variation
            Game.createDeckVariation1();
          } else if(game[1] == 3) {
            //creating deck.
            Game.createDeck();
            //Choosing hand for second variation
            Game.createHandVariation2(scan);
          }

          //creating stacks.
          Game.createStacks();

          //begin the roundNumber-th round.
          ConsoleUtils.clear();
          System.out.println("Here is the round " + roundNumber + " !");
          System.out.println("Press enter to continue.");
          scan.nextLine();
          scan.nextLine();
          ConsoleUtils.clear();

          boolean isPlayersHandsEmpty=false;
          while(!isPlayersHandsEmpty) {
            Game.everyoneChooseACard(scan);
            Game.play(scan);
            //check if every players' hands are empty.
            isPlayersHandsEmpty=Game.isRoundOver();
          }
        roundNumber++;
        Game.resetStacks();
        isGameEnded = Game.checkGameEnds();
        }
        //---END OF GAME---

        Game.allPlayers = ArrayTools.defineScoring(Game.allPlayers);
        Game.displayGameEnd();
        System.out.println("Here is the ranking !");
        int i=1;
        for(Player currentPlayer : Game.allPlayers) {
          System.out.println(i+ ". " + currentPlayer.getName() + " - Jams: " + currentPlayer.getJamPoints());
          i++;
        }
        System.out.println(""); //CRLF
        System.out.println("WELL DONE " + Game.allPlayers[0].getName() + " !");


    }
    scan.close(); //close scanner.
    System.exit(1);
    //WARNING: End of the program.
  }

  /**
   * Display the menu and return the game mode.
   * @return an array of 2 int.
   * [1,1] = Regular Game.
   * [1,2] = Regular Game, first variation.
   * [1,3] = Regular Game, second variation.
   *
   * [2,0] = Stop the program.
   */
  private static int[] displayMenu(Scanner scan) {


    int choiceGame = 0;
    int choiceVariation = 0;
    boolean choiceMade1=false;
    boolean choiceMade2=false;
    do{
      Game.displayTitle();
      System.out.println("Which mode do you want to play ?");
      System.out.println("1. Play");
      System.out.println("2. Exit the game.");
      choiceGame = ScannerHandlerException.scanInt(scan);


      if(choiceGame == 1) {
        do {
          Game.displayTitle();
          System.out.println("Which variation do you want to choose ?");
          System.out.println("1. Regular Game.");
          System.out.println("2. First Variation.");
          System.out.println("3. Second Variation.");
          System.out.println("4. Back.");
          choiceVariation = ScannerHandlerException.scanInt(scan);
          if((choiceVariation == 1) || (choiceVariation == 2) || (choiceVariation == 3)) {
            choiceMade1 = true;
            choiceMade2 = true;
          } else if(choiceVariation == 4) {
            choiceMade2 = true;
          } else {
            ConsoleUtils.errorMessageInput(scan);
          }
        }while(!choiceMade2);
      } else if(choiceGame == 2) {
       choiceMade1 = true;
      }else {
        ConsoleUtils.errorMessageInput(scan);
      }
    } while(!choiceMade1);
    ConsoleUtils.clear();
    int[] returnedGame = new int[2];
    returnedGame[0] = choiceGame;
    returnedGame[1] = choiceVariation;
    return returnedGame;
  }


  /**
   * Method to create every players with their hands.
   * @param scan a Scanner to manage input all along the program.
   */
  private static void createPlayers(Scanner scan) {
    Player[] allPlayers = new Player[playerNb];
    for(int i=0; i<allPlayers.length; i++) {
        String name;
        System.out.println("Choose a name for player "+ (i+1) + ":");
        name = ScannerHandlerException.scanString(scan);
        allPlayers[i] = new Player(name);
      }
    Game.allPlayers = allPlayers;
  }

  /**
   * Method to choose the number of Players.
   * @param scan a Scanner to manage input all along the program.
   */
  private static void chooseNumberOfPlayers(Scanner scan) {
    int nbOfPlayer;
    nbOfPlayer = 0;

    while(nbOfPlayer < 2 || nbOfPlayer > 10) {
      Game.displayTitle();
      System.out.println("WELCOME TO THE 6 NIMMT! GAME ! :) ");
      System.out.println("How many player do you want ?");
      nbOfPlayer = ScannerHandlerException.scanInt(scan);
      if(nbOfPlayer > 10) {
        System.out.println("You can not play with more than 10 players !");
      } else if(nbOfPlayer < 2) {
        System.out.println("It's impossible to play this game with less than 2 players. You seem so lonely :'(");
      } else {
        Game.playerNb = nbOfPlayer;
      }
    }
  }

  /**
   * Generate the deck of the game. Always the same at the begining of a party.
   */
  private static void createDeck() {
    Card[] currentDeck = new Card[DECKSIZE];
    for(int i=0; i<DECKSIZE; i++) {
      if (i+1 == 55) {
        currentDeck[i] = new Card(i+1,6);
      } else if((i+1)%10 == 0) {
        currentDeck[i] = new Card(i+1,3);
      } else if((i+1)%5 == 0) {
        currentDeck[i] = new Card(i+1,2);
      } else if((i+1)%11 == 0) {
        currentDeck[i] = new Card(i+1,5);
      } else {
        currentDeck[i] = new Card(i+1,1);
      }
    }
    Game.deck = currentDeck;
  }

  /**
   * Generate the deck for the first variation of the regular game. It contains the number of player times 10 + 4 cards in ascending order.
   */
  private static void createDeckVariation1() {
    Card[] currentDeck = new Card[playerNb*10+4];
    for(int i=0; i<currentDeck.length; i++) {
      if (i+1 == 55) {
        currentDeck[i] = new Card(i+1,6);
      } else if((i+1)%10 == 0) {
        currentDeck[i] = new Card(i+1,3);
      } else if((i+1)%5 == 0) {
        currentDeck[i] = new Card(i+1,2);
      } else if((i+1)%11 == 0) {
        currentDeck[i] = new Card(i+1,5);
      } else {
        currentDeck[i] = new Card(i+1,1);
      }
    }
    Game.deck = currentDeck;
  }

  /**
   * Fill the hand of a player with 10 randomly picked cards in the deck.
   * @return hand, which is an array of 10 Cards.
   */
  private static Card[] createHand() {
    Card[] hand = new Card[HANDSIZE];
    for(int i=0;i<HANDSIZE;i++) {
      int nbRand = (int)(Math.random() * ((Game.deck.length-1) + 1)); //Generate an index between 0 and the length of deck.
      hand[i] = Game.deck[nbRand]; //Pick card and place it in the hand of the player.
      Game.deck = ArrayUtils.remove(deck,nbRand); //Remove the picked card in the deck.
    }
    return hand;
  }

  /**
   * Allow to choose the hand of every players by second variation rule.
   * @param scan A Scanner to prevent memory leak and InputStream to close unexpectedly.
   */
  private static void createHandVariation2(Scanner scan) {
    boolean isPlayedWisely;
    for(int i=0; i<HANDSIZE; i++) {
      for(Player currentPlayer : Game.allPlayers) {
        isPlayedWisely=false;
        do{
          Game.displayDeckVariation2();
          System.out.println("--------");
          System.out.println(currentPlayer.getName() + " You can choose the cards you want to add in your hand.");
          System.out.println("Here is the deck. (Scroll up if necessary)");
          System.out.println("Choose a card.");
          int choice = ScannerHandlerException.scanInt(scan);
          if(choice < Game.deck.length && choice > 0) {
            currentPlayer.addCardToHand(Game.deck[choice-1]);
            Game.deck = ArrayUtils.remove(Game.deck, (choice-1));
            isPlayedWisely=true;
          } else {
            ConsoleUtils.errorMessageInput(scan);
          }
        }while(!isPlayedWisely);
      }
    }
  }

  private static void displayDeckVariation2() {
    for(int i=0; i<Game.deck.length; i++) {
      System.out.println((i+1) + ". " + Game.deck[i].toString());
    }
  }
  /**
   * Method to display the turn order.
   */
  private static void displayAndSetTurnOrder(Scanner scan) {
    //Clear the console.
    ConsoleUtils.clear();
    Game.allPlayers = ArrayTools.defineTurnPlace(Game.allPlayers);
    System.out.println("Your choices defined the turn order. Here it is:");
    for(int i=0; i<Game.allPlayers.length; i++) {
      System.out.println((i+1) + ". " + Game.allPlayers[i].getName());
    }
    System.out.println("Press any key to continue.");
    scan.nextLine();
  }

  private static void everyoneChooseACard(Scanner scan) {
    for(Player currentPlayer : Game.allPlayers) {
      Game.displayStack();
      currentPlayer.doAnAction(scan);
      System.out.println("---END OF TURN---");
      System.out.println("Press enter to continue.");
      scan.nextLine();
      scan.nextLine();
      ConsoleUtils.clear();
    }
    Game.displayAndSetTurnOrder(scan);
  }

  /**
   * Method to play the game. It makes every players choose their cards, set and display the turn order, put the card in a stack and handle exceptions.
   * @param scan A Scanner to prevent memory leak and InputStream to close unexpectedly.
   */
  private static void play(Scanner scan) {
    for(Player currentPlayer : Game.allPlayers) {
      int[] stacksPlayable = Game.whereCardPlayable(currentPlayer.getChoosedCard());
      if(stacksPlayable.length != 0) {
        if(stacksPlayable.length == 1) {
          if(Game.allStacks[stacksPlayable[0]].isStackFull()) {
            ConsoleUtils.clear();
            System.out.println(currentPlayer.getName() +", the stack "+ (stacksPlayable[0]+1) +" was full. You've gather this stack and create a new one.");
            currentPlayer.setJamPoints(currentPlayer.getJamPoints() + Game.allStacks[stacksPlayable[0]].emptyStack());
            Game.allStacks[stacksPlayable[0]].addCardToStack(currentPlayer.getChoosedCard());
            System.out.println("Press enter to continue.");
            scan.nextLine();
            scan.nextLine();
          } else {
            Game.allStacks[stacksPlayable[0]].addCardToStack(currentPlayer.getChoosedCard());
          }

        } else {
          boolean isPlayedWisely = false;
          do {
            ConsoleUtils.clear();
            System.out.println(currentPlayer.getName() + " we have a problem !");
            System.out.println("Here is your choosed card:");
            System.out.println(currentPlayer.getChoosedCard().toString());
            System.out.println(""); //CRLF
            Game.displayStack();
            System.out.println("You can choose where you can put this card.");
            System.out.println("Here is the stacks where you can add it:");
            for(int i=0; i<stacksPlayable.length;i++) {
              System.out.println("Stack " + (stacksPlayable[i]+1) );
            }
            System.out.println("Which stack do you want to choose ?");

            //Choose the card.
            int choosedStack = ScannerHandlerException.scanInt(scan);
            for(int i=0; i<stacksPlayable.length;i++) {
              if((choosedStack-1) == stacksPlayable[i]) {
                isPlayedWisely = true;
              }
            }
            if(isPlayedWisely) {
              if(Game.allStacks[choosedStack-1].isStackFull()) {
                ConsoleUtils.clear();
                System.out.println(currentPlayer.getName() +", the stack "+ (choosedStack) +" was full. You've gather this stack and create a new one.");
                currentPlayer.setJamPoints(currentPlayer.getJamPoints() + Game.allStacks[choosedStack-1].emptyStack());
                Game.allStacks[choosedStack-1].addCardToStack(currentPlayer.getChoosedCard());
                System.out.println("Press enter to continue.");
                scan.nextLine();
              } else {
                Game.allStacks[stacksPlayable[0]].addCardToStack(currentPlayer.getChoosedCard());
              }
            } else {
              ConsoleUtils.errorMessageInput(scan);
            }
          }while(!isPlayedWisely);
        }
      } else {
        boolean isPlayedWisely=false;
        do {
          ConsoleUtils.clear();
          System.out.println(currentPlayer.getName() + " we have a problem !");
          System.out.println("Here is your choosed card:");
          System.out.println(currentPlayer.getChoosedCard().toString());
          System.out.println(""); //CRLF
          Game.displayStack();
          System.out.println("You can't play this card in any stack !");
          System.out.println("Which stack do you want to gather to restart a new one ?");
          int choosedStack = ScannerHandlerException.scanInt(scan);
          if(choosedStack < 5 && choosedStack > 0) {
            currentPlayer.setJamPoints(Game.allStacks[choosedStack-1].emptyStack());
            Game.allStacks[choosedStack-1].addCardToStack(currentPlayer.getChoosedCard());
            isPlayedWisely=true;
          } else {
            ConsoleUtils.errorMessageInput(scan);
          }
        }while(!isPlayedWisely);
      }
    }
    ConsoleUtils.clear();
  }

  /**
   * Method to display the stacks on the table.
   */
  private static void displayStack() {
    int i=0;
    System.out.println("Here is the stacks on the table:");
    for(Stack currentStack : allStacks) {
      System.out.println("Stack " + (i+1) +":");
      currentStack.displayTable();
      i++;
    }
    System.out.println("-----");
    System.out.println(""); //CRLF
  }

  /**
   * Display the title as ASCII Art.
   */
  private static void displayTitle() {
    ConsoleUtils.clear();
    System.out.println(" ██████╗     ███╗   ██╗██╗███╗   ███╗███╗   ███╗████████╗██╗");
    System.out.println("██╔════╝     ████╗  ██║██║████╗ ████║████╗ ████║╚══██╔══╝██║");
    System.out.println("███████╗     ██╔██╗ ██║██║██╔████╔██║██╔████╔██║   ██║   ██║");
    System.out.println("██╔═══██╗    ██║╚██╗██║██║██║╚██╔╝██║██║╚██╔╝██║   ██║   ╚═╝");
    System.out.println("╚██████╔╝    ██║ ╚████║██║██║ ╚═╝ ██║██║ ╚═╝ ██║   ██║   ██╗");
    System.out.println(" ╚═════╝     ╚═╝  ╚═══╝╚═╝╚═╝     ╚═╝╚═╝     ╚═╝   ╚═╝   ╚═╝");
  }

  /**
   * Display the title which say "GAME ENDED" as ASCII art.
   */
  private static void displayGameEnd() {
    ConsoleUtils.clear();
    System.out.println(" ██████╗  █████╗ ███╗   ███╗███████╗    ███████╗███╗   ██╗██████╗ ███████╗██████╗     ██╗");
    System.out.println("██╔════╝ ██╔══██╗████╗ ████║██╔════╝    ██╔════╝████╗  ██║██╔══██╗██╔════╝██╔══██╗    ██║");
    System.out.println("██║  ███╗███████║██╔████╔██║█████╗      █████╗  ██╔██╗ ██║██║  ██║█████╗  ██║  ██║    ██║");
    System.out.println("██║   ██║██╔══██║██║╚██╔╝██║██╔══╝      ██╔══╝  ██║╚██╗██║██║  ██║██╔══╝  ██║  ██║    ╚═╝");
    System.out.println("╚██████╔╝██║  ██║██║ ╚═╝ ██║███████╗    ███████╗██║ ╚████║██████╔╝███████╗██████╔╝    ██╗");
    System.out.println(" ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝    ╚══════╝╚═╝  ╚═══╝╚═════╝ ╚══════╝╚═════╝     ╚═╝");
  }

  /**
   * Check if the game is ended.
   * @return a boolean. True if the game is ended, false otherwise.
   */
  private static boolean checkGameEnds() {
    boolean isGameEnded=false;
    for(Player currentPlayer : Game.allPlayers) {
      if(currentPlayer.getJamPoints()>= 66) {
        isGameEnded=true;
        break;
      }
    }
    return isGameEnded;
  }

  /**
   * Create every stacks in the at the begining of a round.
   */
  private static void createStacks() {
    for(int i=0; i<ALLSTACKSSIZE; i++) {
      int nbRand = (int)(Math.random() * ((Game.deck.length-1) + 1));
      Stack createdStack = new Stack(Game.deck[nbRand]);
      Game.allStacks = ArrayUtils.add(Game.allStacks,createdStack);
      Game.deck = ArrayUtils.remove(Game.deck, nbRand);
    }
  }

  /**
   * Method to reset every stacks.
   */
  private static void resetStacks() {
    Game.allStacks = new Stack[0];
  }

  /**
   * Check if the greater rule is valid for the card passed as parameter and the stack passed as parameter
   * @param  aCard       Card to put in the stack.
   * @param  passedStack Stack to test if the rule is checked.
   * @return             true if the rule is checked, false otherwise.
   */
  private static boolean isGreaterOK(Card aCard, Stack passedStack) {
    int lastCardIndex = passedStack.getTable().length - 1;
    boolean isCardGreater = passedStack.getTable()[lastCardIndex].getPoints() <= aCard.getPoints();
    return isCardGreater;

  }
  /**
   * Return an array containing the index(es) of playable stack(s) by a card passed as parameter.
   * @param  aCard Card to test if it can be played in any stacks.
   * @return       Return an array containing the index(es) of playable stack(s). (Can be empty).
   */
  private static int[] whereCardPlayable(Card aCard) {
    int[] stackIndex = new int[0];
    //check greater points rule.
    for(int i=0; i<Game.allStacks.length;i++) {
      if(Game.isGreaterOK(aCard, Game.allStacks[i])) {
        stackIndex = ArrayUtils.add(stackIndex, i);
      }
    }
    if(stackIndex.length != 0 ) {
      //check minimal difference rule.
      int[] minimalDifferenceTable = new int[0];
      for(int i=0;i<stackIndex.length;i++) {
        Stack currentStack = Game.allStacks[stackIndex[i]];
        minimalDifferenceTable = ArrayUtils.add(minimalDifferenceTable, currentStack.differencePoints(aCard));
      }
      int lesserMD = ArrayTools.minimalInArray(minimalDifferenceTable);

      //gather indexes which match the two conditions.
      int[] returnedStackIndex = new int[0];
      for(int i=0; i<minimalDifferenceTable.length; i++) {
        if(minimalDifferenceTable[i]==lesserMD) {
          returnedStackIndex = ArrayUtils.add(returnedStackIndex,stackIndex[i]);
        }
      }
      return returnedStackIndex;
    } else {
      int[] returnedStackIndex = new int[0];
      return returnedStackIndex;
    }
  }

  /**
   * Check if the round is over.
   * @return true if the round is over, else otherwise.
   */
  private static boolean isRoundOver() {
    boolean isPlayersHandsEmpty=false;
    for(Player currentPlayer : Game.allPlayers) {
      if(currentPlayer.isHandEmpty()) {
        isPlayersHandsEmpty = true;
      } else {
        isPlayersHandsEmpty = false;
        break;
      }
    }
    return isPlayersHandsEmpty;
  }

  /**
   * Method to get the deck for Junit Tests.
   * @return an array of 104 cards which is the deck.
   */
  public static Card[] getDeck() {
    return Game.deck;
  }

  /**
   * Use the createDeck method, for testing purposes.
   */
  public static void createDeckForTesting() {
    Game.createDeck();
  }

  /**
   * Use the createDeckVariation1 method, for testing purposes.
   */
  public static void createDeckForTestingFirstVariation() {
    Game.createDeckVariation1();
  }

  /**
   * Set the number of player for testing purposes.
   * @param playerNb an int (MUST BE BEETWEEN 2 AND 10 INCLUDED)
   */
  public static void setPlayerNb(int playerNb) {
    Game.playerNb = playerNb;
  }
}
