import ribeyre.sechstnimmt_meta.*;
import java.util.Scanner;
/**
 * Main class to run the game.
 */
public class Main {
  public static void main(String[] args) {
    Game.letsPlay();
  }
}
